# LÖVE2D Adventure Game Engine

A LÖVE2D-based engine for tile-based top-down-view adventure games.

Lots of its code is inspired by USPGAMEDEV's
[backdoor](https://github.com/uspgamedev/backdoor).
The idea is letting the game developer/designer edit the game while playing it in "devmode".
This is made easier with help from the [love-imgui][loveimgui-link] library.
Hopefully this will be possible.

Other used libraries are:

+ [hump](https://github.com/vrld/hump), for tweens, timers, and camera.
+ [lua-toml](https://github.com/jonstoler/lua-toml)
+ [cpml](https://github.com/excessive/cpml)

## Requirements

To use it, you will need a UNIX environment. Sorry, Windows users. Make is beautiful.
For building (you need to compile [love-imgui][loveimgui-link]), you will need:

+ Bash
+ Make
+ CMake
+ Lua (should work, for building, with either v5.2+ and v5.1)
+ LuaJIT dev libs (if love-imgui fails to compile, it'll probably be because of this)
+ C/CXX compiler (gcc/g++ and clang/clang++ both work)

## Install

Clone the repository, and run its `newproject.lua` script with your version of lua,
passing as argument the destination directory (where you want to create your adventure game!).

```lua
lua path/to/repository/newproject.lua path/to/targetdir
```

This lua script also accepts two flags (passed after `path/to/targetdir`):
+ `--nuke` deletes the target directory before building it
+ `--run` after building it, it goes into the directory to configure it

After this, your target directory will become a game project folder. It will contain
a `game` directory, and a `makefile`. If you didn't pass the `--run` flag to the lua script,
you now need to enter the game project folder and run `make`. This will clone the libraries
and put them in the `projectfolder/game/libs` directory.

Incidentally, running `make` will execute the game after everything as well.
You can start making your game after that.

(Well, actually you can't because this is stil in development)

You can also run `make deploy` to create a `game.love` file in the `projectfolder/release` directory.

[loveimgui-link]: https://github.com/slages/love-imgui/
