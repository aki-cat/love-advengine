
--[[ DEPENDENCIES ]]--
local FS = love.filesystem


--[[ CONSTANTS ]]--
local _ASSET_DIR = "assets"


--[[ LOCAL FUNCTIONS ]]--
local _newImage  = love.graphics.newImage
local _newFont   = love.graphics.newFont
local _newSource = love.audio.newSource

local function _getPathToAssetDirectory(asset_type)
  return strf("%s/%s", _ASSET_DIR, asset_type)
end

local function _getPathToAsset(asset_type, filename)
  return strf("%s/%s", _getPathToAssetDirectory(asset_type), filename)
end


--[[ ASSET LOADERS ]]--
local LOADERS = {}

function LOADERS.img(filepath)
  return _newImage(filepath)
end

function LOADERS.font(filepath, size)
  return _newFont(filepath, size)
end

function LOADERS.sfx(filepath)
  return _newSource(filepath, 'static')
end

function LOADERS.bgm(filepath)
  return _newSource(filepath, 'stream')
end


--[[ MODULE METHODS ]]--
local ASSETS = {}

function ASSETS.listFiles(asset_type)
  return FS.getDirectoryItems(_getPathToAssetDirectory(asset_type))
end

function ASSETS.load(asset_type, filename, ...)
  return LOADERS[asset_type](_getPathToAsset(asset_type, filename), ...)
end

return ASSETS

