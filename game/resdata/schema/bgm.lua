
local ASSETS = require 'assets'
local audiofiles = ASSETS.listFiles('bgm')

return {
  { id = 'filename', name = "BackgroundMusic Filename", type = 'enum',
    options = audiofiles, help = "BackgroundMusic filename."
  },
  { id = 'volume', name = "Volume", type = 'slider', range = {0, 1},
    default = 1
  },
  { id = 'pitch', name = "Pitch", type = 'slider', range = {0.25, 4},
    default = 1, power = 2
  },
}

