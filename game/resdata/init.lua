
local DB = require 'game.engine.common.db'

local _BASEDIR = 'resdata'
local _SPECTYPES = {
  'texture', 'sprite', 'tileset', 'font', 'bgm', 'sfx',
}

return DB:new(_BASEDIR, _SPECTYPES)

