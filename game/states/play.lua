
--[[ GAME PLAY STATE ]]--

--[[ DEPENDENCIES ]]--
local INPUT     = require 'engine.input'
local Adventure = require 'engine.universe.adventure'
local DEFS      = require 'engine.universe.definitions'
local MapView   = require 'view.map'


--[[ STATE LOCAL VARIABLES ]]--
local _view
local _adventure


--[[ STATE METHODS ]]--
local state = {}

function state.init()
  _view = {}
end

function state.enter(from, data)
  local data = data or SETTINGS.gamedata
  _adventure = Adventure:new(data)
  _view.map = MapView:new(_adventure)
  _view.map:setLayer("L1")
end

function state.leave()
  _adventure = nil
  _view.map:destroy()
  _view.map = nil
end

function state.update(dt)
  if DEVMODE then
    if INPUT.wasActionReleased('DEBUG_QUIT') then
      return {'QUIT'}
    elseif INPUT.wasActionPressed('DEBUG_DEVMODE') then
      return {'PUSH', GS.devmode}
    end
  end
end

return state

