
--[[ ENCODER MODULE ]]--

--[[ DEPENDENCIES ]]--
local JSON = require 'dkjson'
local fs = love.filesystem

--[[ CONSTS ]]--
local _SOURCE = fs.getSource()
local _DATA_EXTENSION = ".json"

--[[ LOCAL FUNCTIONS ]]--
local function _readStringFrom(filepath)
  local file = assert(fs.newFileData(filepath))
  return file:getString()
end

local function _writeStringTo(filepath, str)
  local file = assert(fs.newFile(filepath, "w"))
  return assert(file:write(str)) and file:close()
end

local function _writeStringToSource(filepath, str)
  local file = assert(io.open(strf("%s/%s", _SOURCE, filepath), "w"))
  return assert(file:write(str) and file:close())
end

local function _deleteFile(filepath)
  return assert(fs.remove(filepath))
end

local function _deleteSourceFile(filepath)
  return assert(os.remove(strf("%s/%s", _SOURCE, filepath)))
end

local function _appendDataExtension(filepath)
  return filepath.._DATA_EXTENSION
end

local function _encode(data) --> string
  return JSON.encode(data, { indent = true })
end

local function _decode(str) --> table
  return JSON.decode(str)
end

--[[ MODULE METHODS ]]--
local ENCODER = {}

function ENCODER.loadData(filepath)
  local path = _appendDataExtension(filepath)
  return _decode(_readStringFrom(path))
end

function ENCODER.writeData(filepath, data, to_source)
  local path = _appendDataExtension(filepath)
  return to_source
         and _writeStringToSource(path, _encode(data))
         or _writeStringTo(path, _encode(data))
end

function ENCODER.deleteData(filepath, from_source)
  local path = _appendDataExtension(filepath)
  return from_source
         and _deleteSourceFile(path)
         or _deleteFile(path)
end

function ENCODER.isDataFile(filepath)
  local path = _appendDataExtension(filepath)
  return fs.isFile(path)
end

return ENCODER

