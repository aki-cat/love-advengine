
local IMGUI = require 'imgui'

local _EMPTY_STRING = ""

return function(target, key)
  local id = key.id
  local default = key.default or false

  target[id] = target[id] or default

  local changed, new = IMGUI.Checkbox(_EMPTY_STRING, target[id])
  if changed then
    target[id] = new
  end
end
