
local IMGUI = require 'imgui'

local _EMPTY_STRING = ""

local min, max = math.min, math.max
local function _range(value, range)
  return max(range[1] or value, min(range[2] or value, value))
end

return function(target, key)
  local id = key.id
  local range = key.range or table.empty
  local default = _range(key.default or 0, range)

  target[id] = target[id] or default

  local changed, new = IMGUI.InputInt(_EMPTY_STRING, target[id])
  if changed then
    target[id] = _range(new, range)
  end
end
