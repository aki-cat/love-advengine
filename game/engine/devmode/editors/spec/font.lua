
--[[ DEPENDENCIES ]]--
local IMGUI = require 'imgui'

local Prototype = require 'engine.common.prototype'
local LAYOUT    = require 'engine.devmode.layout'
local INPUTS    = require 'engine.devmode.inputs'
local SCHEMA    = require 'resdata.schema.font'
local RESDATA   = require 'resdata'
local RESLOADER = require 'resources'


--[[ CONSTANTS ]]--
local _WIDTH, _HEIGHT
local _SAMPLETEXT = [=[
the quick brown fox jumped over the lazy dog
THE QUICK BROWN FOX JUMPED OVER THE LAZY DOG
áéíóúàèìòùäëïöüÁÉÍÓÚÀÈÌÒÙÄËÏÖÜ
0123456789.,;`"'/!?@#$%^&*()]=]

--[[ EDITOR METHODS ]]--
local Editor = Prototype:new()

function Editor:init(specname)
  _WIDTH, _HEIGHT = love.graphics.getDimensions()
  self.name = strf("Edit font: %s", specname)
  self.size = "BIG"
  self.x = _WIDTH - LAYOUT.MARGIN_LEFT - LAYOUT[self.size].MAX_W

  self.target = RESDATA:loadSpec('font', specname)
  self.loader = false
  self.specname = specname
end

function Editor:refresh()
  if self.loader then self.loader:refresh() end
end

function Editor:update(dt)
  local action

  if IMGUI.Button("Refresh##top") then
    self:refresh()
  end
  IMGUI.SameLine()
  if IMGUI.Button("Save##top") then
    self:refresh()
    RESDATA:save('font', self.specname)
  end

  IMGUI.Separator()

  INPUTS("Font Info", self.target, SCHEMA)

  if not self.loader and self.target.filename then
    self.loader = RESLOADER.load('font', self.specname)
  end

  if self.loader then self:previewFont() end

  IMGUI.Separator()

  if IMGUI.Button("Refresh##bottom") then
    self:refresh()
  end
  IMGUI.SameLine()
  if IMGUI.Button("Save##bottom") then
    self:refresh()
    RESDATA:save('font', self.specname)
  end

  return action
end

function Editor:generatePreview(width, height)
  local g = love.graphics
  local canvas = g.newCanvas(width, height)

  g.setColor(255, 255, 255)
  canvas:renderTo(function ()
    g.setColor(255, 255, 255)
    g.printf(_SAMPLETEXT, 12, 12, width-24)
  end)

  return g.newImage(canvas:newImageData())
end

function Editor:previewFont()
  local loader = self.loader
  local width = LAYOUT[self.size].MAX_W - 24
  local lh = loader:getAttr("Height")*loader:getAttr("LineHeight")
  local breaklines = #(select(2, loader:getAttr("Wrap", _SAMPLETEXT, width-24)))
  local height = lh * (breaklines + 3)
  local preview = self.preview

  loader:setAsCurrent()
  if not preview or preview:getHeight() ~= height then
    preview = self:generatePreview(width, height)
    self.preview = preview
  end

  IMGUI.Image(preview, width, height)
end

return Editor
