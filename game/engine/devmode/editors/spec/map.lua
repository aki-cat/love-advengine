
--[[ DEPENDENCIES ]]--
local IMGUI = require 'imgui'

local LAYOUT    = require 'engine.devmode.layout'
local Prototype = require 'engine.common.prototype'


--[[ CONSTANTS ]]--


--[[ EDITOR METHODS ]]--
local Editor = Prototype:new()

function Editor:init(specname)
  self.name = strf("Edit map: %s", specname)
  self.size = "SMALL"
end

function Editor:update(dt)
  local action
  return action
end

return Editor
