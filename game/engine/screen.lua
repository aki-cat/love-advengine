
local SCREEN = {}

local _LAYERS = {
  "BG",
  "L1",
  "L2",
  "L3",
  "L4",
  "GUI",
}

local _layer
local _dirty

function SCREEN.add(obj, lname)
  assert(_layer[lname], strf("No such layer %s", lname))
  _layer[lname][obj] = true
end

function SCREEN.remove(obj, lname)
  assert(_layer[lname], strf("No such layer %s", lname))
  _layer[lname][obj] = nil
end

function SCREEN.load()
  _layer = {}
  for _,lname in ipairs(_LAYERS) do
    _layer[lname] = {}
  end
end

function SCREEN.setDirty()
  _dirty = true
end

function SCREEN.update()
  if _dirty then
    for _,lname in ipairs(_LAYERS) do
      local delete = {}
      for obj in pairs(_layer[lname]) do
        if obj:isDead() then delete[obj] = true end
      end
      for obj in pairs(delete) do
        _layer[lname][obj] = nil
      end
    end
    _dirty = false
  end
end

function SCREEN.draw(g)
  for _,lname in ipairs(_LAYERS) do
    for obj in pairs(_layer[lname]) do
      if not obj:isInvisible() and not obj:isDead() then
        obj:draw(g)
      end
    end
  end
end

return SCREEN

