
--[[ UTILITY HELPERS ]]--
require 'engine.misc.util'


--[[ GLOBALS ]]--
DEVMODE = pcall(require, 'imgui')
GS = require 'engine.common.modulepack' 'states'
GS.devmode = require 'engine.devmode.state'
SETTINGS = false


--[[ REQUIRED MODULES ]]--
local INPUT  = require 'engine.input'
local STATE  = require 'engine.state'
local SCREEN = require 'engine.screen'
local CONSTS = require 'engine.consts'

local ENCODER     = require 'engine.misc.encoder'
local RANDOM      = require 'engine.common.random'
local TIMER       = require 'engine.common.timer'
local GameElement = require 'engine.common.gameelement'

local GAMEDATA    = require 'gamedata'
local RESDATA     = require 'resdata'
local RESLOADER   = require 'resources'

local GRAPHICS = love.graphics
local WINDOW = love.window
local FS = love.filesystem


--[[ INIT CONFIG ]] --
local function _initConfig()
  -- load settings from memory or disk
  SETTINGS = SETTINGS or ENCODER.loadData(CONSTS.SETTINGS_FILEPATH)

  -- global settings
  FS.setIdentity(SETTINGS.id)
  WINDOW.setTitle(SETTINGS.title)

  -- window settings
  local width, height = unpack(SETTINGS.display.dimensions)
  local mode = SETTINGS.display.mode
  WINDOW.setMode(width, height, mode)

  -- graphics settings
  local filter = SETTINGS.graphics['default-filter']
  GRAPHICS.setDefaultFilter(filter, filter)
  love.mouse.setVisible(false)
end


--[[ INIT MODULES ]] --
local function _initModules()
  RANDOM.init()
  SCREEN.load()
  INPUT.setup(SETTINGS.controls)
  RESLOADER.init()
  STATE.start(GS.start)
end


--[[ LOAD GAME ]]--
function love.load(...)
  print("\n\n> START GAME\n[ DEVMODE IS ON ]")
  _initConfig()
  _initModules()
end


--[[ UPDATE LOOP ]]--
function love.update(dt)
  TIMER.update(dt)
  STATE.update(dt)
  INPUT.flush()
  SCREEN.update()
  GameElement.clean()
end


--[[ RENDER LOOP ]]--
function love.draw()
  SCREEN.draw(GRAPHICS)
  STATE.check()
end


--[[ QUIT EVENT ]]--
function love.quit()
  return false
end


--[[ RESET EVENT ]]--
function love.reset()
  GameElement.clearAll()
  GAMEDATA:refresh()
  RESDATA:refresh()
  love.load()
end
