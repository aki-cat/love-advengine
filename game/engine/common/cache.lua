
--[[ DEPENDENCIES ]]--
local Prototype = require 'engine.common.prototype'


--[[ LOCAL FUNCTIONS ]]--
local function _getLeastUsedField(count)
  local smallest
  for field, usage in pairs(count) do
    smallest = smallest or field
    if usage < count[smallest] then
      smallest = field
    end
  end
  return smallest
end


--[[ CACHE MODULE ]]--
local Cache = Prototype:new()

-- maxsize is number of items allowed at the same time,
-- but it doesn't take into consideration the amount
-- of memory each item consumes!
function Cache:init(maxsize)
  self.maxsize = maxsize
  self.size = 0
  self.count = {}
  self.storage = {}
end

function Cache:get(field)
  if not self.storage[field] then return end
  self.count[field] = self.count[field] + 1
  return self.storage[field]
end

function Cache:set(field, data)
  -- if field is not already set
  if self.storage[field] then return end

  -- check if there's room for more data
  if self.size < self.maxsize then
    -- if there is, just add to the size
    self.size = self.size + 1
  else
    -- if there is no more room, we substitue the least used field
    local least = _getLeastUsedField(self.count)
    self.count[least] = nil
    self.storage[least] = nil
  end

  -- set new field
  self.count[field] = 0
  self.storage[field] = data
end

return Cache

