
local CONSTS = {}

CONSTS.SETTINGS_FILEPATH = "settings"
CONSTS.COLLISION_TYPE = {
  'FLOOR', 'WALL', 'W_UL', 'W_UR', 'W_DR', 'W_DL',
  FLOOR = '.',
  WALL  = '#',
  W_UL  = 'y',
  W_UR  = 'u',
  W_DR  = 'n',
  W_DL  = 'b',
}

return CONSTS
